# coding=utf-8
from bs4 import BeautifulSoup
from bson.dbref import DBRef
from lang.Es import cadenas
import requests
import datetime
import pymongo
import json
import re

class MercadoLibreAPI():

    def __init__(self):
        self.sesion = requests.Session()
        self.urlRegion = "https://api.mercadolibre.com/sites/MLA/search?category=MLA1459&state=TUxBUENIVXQxNDM1MQ"
        self.urlPaginado = "https://api.mercadolibre.com/sites/MLA/search?category=MLA1459&offset=%s&state=TUxBUENIVXQxNDM1MQ"
        self.urlDetalle = "https://api.mercadolibre.com/items/"
        self.urlDescripcion = "https://api.mercadolibre.com/items/%s/descriptions/%s"

        self.limit = 50
        self.offset = 0
        self.paginas = 1

        self.totalResultados = None 
        self.totalPaginas = None 
        self.response = None 
        self.cliente = None
        self.db = None
        self.colData = None
        
        # seteamos user-agent
        self.header = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36'}  
        print "Definimos User-Agent %s " % (self.header)

        # iniciamos
        self.inicializar()
        
    def inicializar(self):
        print "Inicializar App"
        # inicializo mongoDB
        self.inicializarDb()
       
        # primer peticion para traer datos de configuracion
        self.response = self.sesion.get(self.urlRegion, headers=self.header)

        if self.response.status_code == 200:
            resJson = json.loads(self.response.text)
            self.procesarJson(resJson)
        else:
            raise ValueError(cadenas['error_response_none'] % (self.urlRegion,self.response.status_code))
        
    def inicializarDb(self):
        print "Inicializar DB"
        self.cliente = pymongo.MongoClient("mongodb://localhost:27017/")
        self.db = self.cliente["inmuebles_db"]
        self.colData = self.db["inmuebles"]
    
    def procesarJson(self,res):
        print "Iniciamos Proseso"
        if res != None:
            self.totalResultados = res["paging"]["total"] 
            self.totalPaginas = (self.totalResultados / res["paging"]["limit"]) + 1
            
            procesar = True 
            
            while procesar:
                
                print "Comenzamos a procesar paginas %s/%s" % (self.paginas,self.totalPaginas)
                
                for r in res["results"]:
                    try:
                        detalles = {}    
                        detalles["fecha_acceso"] = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                        detalles["fuente"] = r["permalink"]
                        detalles["id"] = r["id"]

                        # volvemos a llamar a la api con el detalle   
                        urlDetalle =  "%s%s" % (self.urlDetalle, r["id"])
                        print "Obtenemos detalles de la publicacion %s"%(urlDetalle)        
                        dresponse = self.sesion.get(urlDetalle, headers=self.header)
                        if dresponse.status_code == 200:
                            d = json.loads(dresponse.text)
                        else:
                            d = None

                        #obtenemos descripcion
                        if d["descriptions"] != None and len(d["descriptions"]) != 0:
                            desc = d["descriptions"][0]["id"].split("-")     
                            urlDescripcion = self.urlDescripcion % (r["id"],desc[1])
                            print "Obtenemos url descripcion %s"%(urlDescripcion)
                            descresponse = self.sesion.get(urlDescripcion, headers=self.header)
                            if descresponse.status_code == 200:
                                djson = json.loads(descresponse.text)
                                descri = djson["plain_text"] 
                            else:
                                descri = None 
                        else:
                            descri = None 


                        detalles["tipologia"] = None    #
                        detalles["titulo"] = r["title"]
                        detalles["direccion"] = r["location"]["address_line"]
                        detalles["descripcion"] = descri
                        detalles["precio"] = r["price"]
                        detalles["fecha_publicacion"] = d["date_created"]
                        detalles["lat"] = r["location"]["latitude"]
                        detalles["long"] = r["location"]["longitude"]
                        detalles["vendedor"] = r["seller_contact"]["contact"]
                        
                        # buscamos imagenes
                        pic = {}
                        i = 0
                        for img in d["pictures"]:
                            pic[str(i)] = img["secure_url"]
                            i+=1        
                        detalles["images"] = pic

                        detalles["barrio"] = None 
                        detalles["operacion"] = None 
                        detalles["moneda"] = r["currency_id"]
                        detalles["pais"] = r["location"]["country"]["name"] 
                        detalles["provincia"] = r["location"]["state"]["name"]
                        detalles["partido"] = r["location"]["neighborhood"]["name"] 
                        detalles["localidad"] = r["location"]["city"]["name"] 

                        # attrs
                        detalles["antiguedad"] = None   #
                        detalles["agua"] = None         #
                        detalles["electricidad"] = None #
                        detalles["gas"] = None          #
                        detalles["cloacas"] = None 
                        detalles["pileta"] = None       #
                        detalles["cochera"] = None      #
                        detalles["ambientes"] = None    #
                        detalles["banios"] = None       #
                        detalles["lote_m2"] = None      #
                        detalles["m2_cubiertos"] = None #
                        detalles["dormitorios"] = None  #
                        detalles["luz"] = None          #
                        detalles["telefono"] = None     #
                        detalles["sup_terreno"] = None  #

                        #buscamos attrs y si existen sobre escribimos
                        for attr in d["attributes"]:

                            if attr["id"] == "PROPERTY_AGE":
                                detalles["antiguedad"] = attr["value_name"]

                            if attr["id"] == "HAS_ELECTRIC_LIGHT":
                                detalles["electricidad"] = attr["value_name"]
                                detalles["luz"] = attr["value_name"]

                            if attr["id"] == "HAS_NATURAL_GAS":
                                detalles["gas"] = attr["value_name"]

                            if attr["id"] == "HAS_TAP_WATER":
                                detalles["agua"] = attr["value_name"]

                            if attr["id"] == "HAS_TELEPHONE_LINE":
                                detalles["telefono"] = attr["value_name"]
                                                                        
                            if attr["id"] == "TOTAL_AREA":
                                if attr["value_struct"] != None:
                                    detalles["lote_m2"] = attr["value_struct"]["number"]
                                    detalles["sup_terreno"] = attr["value_struct"]["number"]
                                else:
                                    detalles["lote_m2"] = attr["value_name"]
                                    detalles["sup_terreno"] = attr["value_name"]

                            if attr["id"] == "COVERED_AREA":
                                if attr["value_struct"] != None:
                                    detalles["m2_cubiertos"] = attr["value_struct"]["number"]
                                else:
                                    detalles["m2_cubiertos"] = attr["value_name"]    
                                
                            if attr["id"] == "BEDROOMS":
                                detalles["dormitorios"] = attr["value_name"]

                            if attr["id"] == "BEDROOMS":
                                detalles["dormitorios"] = attr["value_name"]

                            if attr["id"] == "HAS_SWIMMING_POOL":
                                detalles["pileta"] = attr["value_name"]

                            if attr["id"] == "ROOMS":
                                detalles["ambientes"] = attr["value_name"]

                            if attr["id"] == "FULL_BATHROOMS":
                                detalles["banios"] = attr["value_name"]

                            if attr["id"] == "OPERATION":
                                detalles["operacion"] = attr["value_name"]

                            if attr["id"] == "PROPERTY_TYPE":
                                detalles["tipologia"] = attr["value_name"]
                                                                                    
                        detalles["orientacion"] = None                     
                        detalles["soporte"] = None
                        # residencial comercial agroindustrial etc
                        detalles["uso_real"] = None
                        detalles["uso_potencial"] = None
                        detalles["positivos"] = None
                        detalles["negativos"] = None
                        # lat long
                        detalles["ubicacion"] = None
                        detalles["constructora"] = None
                        detalles["financiamiento"] = None
                        detalles["edificio"] = None
                        # plan desarrollo urbano
                        detalles["zona_segun_pdu"] = None
                        # FOT Factor de ocupacion total (Establece la superficie máxima construible)
                        detalles["fot"] = None
                        # FOS Factor de ocupación de suelo (Es la relación entre la superficie máxima del suelo ocupado por el edificio y la superficie de la parcela)
                        detalles["fos"] = None
                        detalles["amenidades"] = None
                        # distancia en km
                        detalles["distancia_centro"] = None
                        detalles["incidencia_precio_suelo_precio_total"] = None
                        detalles["registro_unico"] = None

                        item = {
                            "fecha": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                            "link": r["permalink"],
                            "detalles": detalles,
                            "raw": str(r)
                        }
                        self.colData.insert_one(item)
                    except:
                        print "Error con el item %s"%(r["id"])    

                if self.paginas == self.totalPaginas:            
                    procesar = False
                    break
                else: 
                    self.offset = self.paginas * self.limit
                    siguientePagina = self.urlPaginado % (self.offset)
                    print "Siguiente pagina %s"%(siguientePagina)
                    self.response = self.sesion.get(siguientePagina, headers=self.header)
                    if self.response.status_code == 200:
                        res = json.loads(self.response.text)
                        self.paginas+=1
                    else:
                        procesar=False
                        raise ValueError(cadenas['error_response_paginado'] % siguientePagina)
   
                
           