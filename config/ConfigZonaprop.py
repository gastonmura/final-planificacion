# coding=utf-8
import datetime

configZonaprop = {
        # encode
        "encode" : "utf-8",
        
        # db config
        "db_conn" : "mongodb://localhost:27017/",
        "db_mane" : "inmuebles_db",
        "col_item" : "inmuebles",
        
        # urls scrap
        "url_base" : "https://www.zonaprop.com.ar",
        "url_region" : "https://www.zonaprop.com.ar/inmuebles-chubut.html",
        "url_paginado" : "https://www.zonaprop.com.ar/inmuebles-chubut-pagina-%s.html",
        "paginado_inicio" : 2,
        "procesar_pagina_inicial": True, # si seteo esta config en falso el scrp comeinza directo desde "pagina_inicio"
        # scrap lista de publicaciones
        "publicaciones" : "div[class='list-card-container  ']",
        
         # total resultados, cantidad de publicaciones del sitio, datos estadistico
        "total_resultados" : "p[class='list-result-title']",

        # modo select|find
        # elem nodo
        # class selector tipo css si el modo es select o una o mas clases si el modo es find
        # i indice donde esta el numero de total paginas
        "total_paginas" : 45,

        # linkdel detalle de la publicacion
        "link_detalle" : { "elem":"div", "class":"postingCard", "attr":"data-to-posting","add_url_base":True},
        
        
        #####################################################################################
        #                                                                                   #
        #   Configuracion para obtener datos del listado de publicaciones del inmueble      #
        #                                                                                   #            
        #####################################################################################
        "detalle_listado" : {
            # fecha en que se scrapeo
            "fecha_acceso" : {"modo":"hardcode", "s": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))},
            # de donde se scrapeo url
            "fuente" : {"modo":"hardcode", "s":"https://www.zonaprop.com.ar"},
        },
        
        #####################################################################################                                                                                #
        #                                                                                   #
        #       Configuracion para obtener datos de la vista detalles del inmueble          #
        #                                                                                   #            
        #####################################################################################
        "detalles" : {
            "id" : {"modo":"find", "elem":"body", "class":"ZP","re":"\'idAviso\'': ([0-9]+)"},
             # Tipo de inmueble, casa, dpto, loft, etc
            "tipologia" : { "modo":"select_one", "selector":"#ga-dimension-ficha", "attr":"data-tipo-propiedad" },
            "titulo" : { "modo":"find", "elem":"span", "class":"postingCardLocationTitle" },
            "direccion" : { "modo":"find", "elem":"h3", "class":"titlebar__address" },
            "descripcion" : {"modo":"find", "elem":"div", "class":"property-description"},
            "precio" : { "modo":"find", "elem":"p", "class":"titlebar__price"},
            "fecha_publicacion" : None,
            "lat" : {"modo":"find", "elem":"div", "class":"leaflet-container","attr":"data-latitude"},
            "long" : {"modo":"find", "elem":"div", "class":"leaflet-container","attr":"data-longitude"},
            "vendedor":{"modo":"find", "elem":"p", "class":"agent-bottom__agent-name"},    
            "images" : { "modo":"select", "selector":".property-slider img[data-src]","attr":"data-src"},
            "barrio" : { "modo":"select_one", "selector":"#ga-dimension-ficha", "attr":"data-barrio" },
            "operacion" : { "modo":"select_one", "selector":"#ga-dimension-ficha", "attr":"data-tipo-operacion" },
            
            "moneda": { "modo":"select_one", "selector":"#ga-dimension-ficha", "attr":"data-moneda" },
            "pais" : { "modo":"select_one", "selector":"#ga-dimension-ficha", "attr":"data-pais" },
            "provincia" : { "modo":"select_one", "selector":"#ga-dimension-ficha", "attr":"data-provincia" },
            #dpto, provincia
            "partido"  : { "modo":"select_one", "selector":"#ga-dimension-ficha", "attr":"data-partido" },
            "localidad" : { "modo":"select_one", "selector":"#ga-dimension-ficha", "attr":"data-localidad" },   
            
            ###########################
            ## Claves No Encontradas ##
            ## que trateremos de     ##
            ## scrapear de las desc  ##
            ###########################
            "antiguedad" : {"modo":"find", "elem":"div", "class":"property-description","re":"([0-9]+)\s+antig|antiguedad.*\s+([0-9]+)"},
            "agua" : {"modo":"find", "elem":"div", "class":"property-description","re":"(agua)"},
            "electricidad" : {"modo":"find", "elem":"div", "class":"property-description","re":"(electricidad)"},
            "gas" : {"modo":"find", "elem":"div", "class":"property-description","re":"(gas)"},
            "cloacas" : {"modo":"find", "elem":"div", "class":"property-description","re":"(cloacas)"},
            "pileta" : {"modo":"find", "elem":"div", "class":"property-description","re":"(pileta|picina)"},
            "cochera" : {"modo":"find", "elem":"div", "class":"property-description","re":"(cochera|garage)"},
            "ambientes" :  {"modo":"find", "elem":"div", "class":"property-description","re":"([0-9]+)\s+ambiente|ambiente.*\s+([0-9]+)"},
            "banios" : {"modo":"find", "elem":"div", "class":"property-description","re":"Ba.*os:\s+([0-9]+)"},
            "lote_m2" :  {"modo":"find", "elem":"div", "class":"property-description","re":"Total:\s+([0-9.]+)"},
            "m2_cubiertos" : {"modo":"find", "elem":"div", "class":"property-description","re":"Cubierta:\s+([0-9.]+)"},
            "dormitorios" : {"modo":"find", "elem":"div", "class":"property-description","re":"Dormitorios|Habitaciones:\s+([0-9]+)"},
            # norte, sur, sureste, etc
            "orientacion" : {"modo":"find", "elem":"div", "class":"property-description","re":"Disposici.*n:\s+(.*)"},
            "luz" : {"modo":"find", "elem":"div", "class":"property-description","re":"(luz)"},
            "telefono" : {"modo":"find", "elem":"div", "class":"property-description","re":"(tel.*fono)"},
            "sup_terreno" : {"modo":"find", "elem":"div", "class":"property-description","re":"Terreno:\s+([0-9.]+)"},

            ###########################
            ## Claves No Encontradas ##
            ###########################
            # url tambien
            "soporte" : None,
            # residencial, comercial, agroindustrial, etc
            "uso_real" : None,
            "uso_potencial" : None,
            "positivos" : None,
            "negativos" : None,
            # lat, long
            "ubicacion" : None,
            "constructora" : None,
            "financiamiento" : None,
            "edificio" : None,
            # plan desarrollo urbano
            "zona_segun_pdu" : None,
            # FOT Factor de ocupacion total (Establece la superficie máxima construible)
            "fot" : None,
            # FOS Factor de ocupación de suelo (Es la relación entre la superficie máxima del suelo ocupado por el edificio y la superficie de la parcela)
            "fos" : None,
            "amenidades" : None,
            # distancia en km
            "distancia_centro" : None,
            "incidencia_precio_suelo_precio_total" : None,
            "registro_unico" : None, 
        }
    }