# coding=utf-8
import datetime

configProperati = {
        # encode
        "encode" : "utf-8",

        # db config
        "db_conn" : "mongodb://localhost:27017/",
        "db_mane" : "inmuebles_db",
        "col_item" : "inmuebles",
        
        # urls scrap
        "url_base" : "https://www.properati.com.ar",
        "url_region" : "https://www.properati.com.ar/chubut/",
        "url_paginado" : "https://www.properati.com.ar/chubut/%s",
        "paginado_inicio" : 2,
        "procesar_pagina_inicial": True, # si seteo esta config en falso el scrp comeinza directo desde "pagina_inicio"
        # scrap lista de publicaciones
        "publicaciones" : "article[id*='property_']",

        # total resultados, cantidad de publicaciones del sitio, datos estadistico
        "total_resultados" : "p[class='results-count']",

        # modo select|find
        # elem nodo
        # class selector tipo css si el modo es select o una o mas clases si el modo es find
        # i indice donde esta el numero de total paginas
        "total_paginas" : { "modo":"select", "elem":"li", "class":"div.pagination.pagination-centered ul li a" , "i":-2 },

        # linkdel detalle de la publicacion
        "link_detalle" : { "elem":"span", "class":"link item-url", "attr":"href","add_url_base":False},
        
        #####################################################################################
        #                                                                                   #
        #   Configuracion para obtener datos del listado de publicaciones del inmueble      #
        #                                                                                   #            
        #####################################################################################
        "detalle_listado" : {
            "id" : { "modo":"root", "attr":"data-id" },
            # Tipo de inmueble, casa, dpto, loft, etc
            "tipologia" : {"modo":"find", "elem":"p", "class":"property-type"},
            "direccion" : { "modo":"find", "elem":"h3", "class":"address" },
            # fecha en que se scrapeo
            "fecha_acceso" : {"modo":"hardcode", "s": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))},
            # de donde se scrapeo url
            "fuente" : {"modo":"hardcode", "s":"https://www.properati.com.ar/"},
            # adds
            "ambientes":{"modo":"select_one", "selector":".rooms", "re":"([0-9]+) ambiente"},
            "titulo" : { "modo":"find", "elem":"a", "class":"item-url", "attr":"title" },
            "images" : { "modo":"select", "selector":".carousel-inner div img","re":"src=\"(.+)\""},
        },
        
        #####################################################################################                                                                                #
        #                                                                                   #
        #       Configuracion para obtener datos de la vista detalles del inmueble          #
        #                                                                                   #            
        #####################################################################################
        "detalles" : {
            "banios" : {"modo":"select_one", "selector":"div[class*='StyledTypologyBlock']","re":"([0-9]+)\s?ba.*o"},
            "lote_m2" : {"modo":"select_one", "selector":"div[class*='StyledTypologyBlock']","re":"([0-9]+) m²Totales"},
            "m2_cubiertos" : {"modo":"select_one", "selector":"div[class*='StyledTypologyBlock']","re":"([0-9]+) m²Cubiertos"},
            "descripcion" : {"modo":"find", "elem":"div", "class":"child-wrapper"},
            "precio" : { "modo":"find", "elem":"span", "class":"StyledPrice-sc-1wixp9h-0 bZCCaW"},
            "fecha_publicacion" : {"modo":"select_one", "selector":".StyledContentSeller-sc-1yzimq1-2 div p", "re":"Publica desde el ([0-9]+/[0-9]+/[0-9]+)"},
            "lat" : {"modo":"select_one", "selector":"#ad-schema", "re":"latitude\": ([0-9-\.]+)"},
            "long" : {"modo":"select_one", "selector":"#ad-schema", "re":"longitude\": ([0-9-\.]+)"},
            "vendedor":{"modo":"select_one", "selector":".StyledContentSeller-sc-1yzimq1-2 div h2"},    
            "operacion" : { "modo":"select_one", "selector":".StyledTags-c1w234-1.cUmmku span a" },
            
            ###########################
            ## Claves No Encontradas ##
            ## que trateremos de     ##
            ## scrapear de las desc  ##
            ###########################
            "dormitorios" : {"modo":"find", "elem":"div", "class":"child-wrapper","re":"([0-9]+)\s+dormitorio|dormitorio.?\s+([0-9]+)"},
            "antiguedad" : {"modo":"find", "elem":"div", "class":"child-wrapper","re":"([0-9]+)\s+antig|antiguedad\s+([0-9]+)"},
            "agua" : {"modo":"find", "elem":"div", "class":"child-wrapper","re":"(agua)"},
            "electricidad" :  {"modo":"find", "elem":"div", "class":"child-wrapper","re":"(electricidad)"},
            "gas" :  {"modo":"find", "elem":"div", "class":"child-wrapper","re":"(gas)"},
            "cloacas" :  {"modo":"find", "elem":"div", "class":"child-wrapper","re":"(cloacas)"},
            "pileta" :  {"modo":"find", "elem":"div", "class":"child-wrapper","re":"(pileta)"},
            "cochera" :  {"modo":"find", "elem":"div", "class":"child-wrapper","re":"(cochera)"},
            "luz" : {"modo":"find", "elem":"div", "class":"child-wrapper","re":"(luz)"},
            "telefono" : {"modo":"find", "elem":"div", "class":"child-wrapper","re":"(tel.*fono)"},
            "sup_terreno" : {"modo":"find", "elem":"div", "class":"child-wrapper","re":"Terreno:\s+([0-9.]+)"},

            ###########################
            ## Claves No Encontradas ##
            ###########################
            "moneda":None,
            "pais" : None,
            "provincia" : None,
            #dpto, provincia
            "partido"  : None,
            "localidad" : None,
            # url tambien
            "soporte" : None,
            "barrio" : None,
            # residencial, comercial, agroindustrial, etc
            "uso_real" : None,
            "uso_potencial" : None,
            "positivos" : None,
            "negativos" : None,
            # lat, long
            "ubicacion" : None,
            "constructora" : None,
            "financiamiento" : None,
            "edificio" : None,
            # plan desarrollo urbano
            "zona_segun_pdu" : None,
            # FOT Factor de ocupacion total (Establece la superficie máxima construible)
            "fot" : None,
            # FOS Factor de ocupación de suelo (Es la relación entre la superficie máxima del suelo ocupado por el edificio y la superficie de la parcela)
            "fos" : None,
            "amenidades" : None,
            # norte, sur, sureste, etc
            "orientacion" : None,
            # distancia en km
            "distancia_centro" : None,
            "incidencia_precio_suelo_precio_total" : None,
            "registro_unico" : None, 
        }
    }