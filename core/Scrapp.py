# coding=utf-8
from bs4 import BeautifulSoup
from bson.dbref import DBRef
from lang.Es import cadenas
import requests
import datetime
import pymongo
import json
import re

class Scrapp():

    # TODO control de errores
    # TODO control de parametros de configuracion
    # TODO ya soporta regex los metodos find_all, y se les puede pasar una funcion para aplicar
    # TODO refactor forma de calcular el total de paginas

    def __init__(self,config):
        self.config = config
        self.sesion = requests.Session()
        self.urlBase = self.config['url_base']
        self.urlRegion = self.config['url_region']
        self.urlPaginado = self.config['url_paginado']
        self.totalResultados = None 
        self.totalPaginas = None 
        self.response = None 
        self.cliente = None
        self.db = None
        self.colData = None
        self.bs = None

        # seteamos user-agent
        self.header = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36'}  
        print "Definimos User-Agent %s " % (self.header)

        # iniciamos
        self.inicializar()
        
    def inicializar(self):
        print "Inicializar App"
        # inicializo mongoDB
        self.inicializarDb()
       
        # primer peticion para traer datos de configuracion
        self.response = self.sesion.get(self.urlRegion, headers=self.header)

        if self.response.status_code == 200:
            # buscamos datos de configuracion
            self.bs = BeautifulSoup(self.response.content, 'html.parser')
            
            # cantidad de resultados de la busqueda
            # TODO controles de resultados
            aux = self.bs.select(self.config['total_resultados'])[0].get_text()
            self.totalResultados = re.search("([0-9.]+)", aux)
            print "Total resultados %s" % (self.totalResultados.group(0))


            # cantidad de paginas de la busqueda
            # TODO controles de resultados
            if self.config['total_paginas']['modo'] == 'select':
                self.totalPaginas = self.bs.select(
                    self.config['total_paginas']['class']
                )[self.config['total_paginas']['i']].get_text()
            elif self.config['total_paginas']['modo'] == 'find':
                self.totalPaginas = self.bs.find_all(
                    self.config['total_paginas']['elem'],
                    class_=self.config['total_paginas']['class']
                )[self.config['total_paginas']['i']].get_text()
            else:
                # asignamos el numero de paginas directamente
                self.totalPaginas = self.config['total_paginas']

            print "Total paginas %s" % (self.totalPaginas)

            # iniciamos el scrapeo
            self.scrap()
        
        else:
            raise ValueError(cadenas['error_response_none'] % (self.urlRegion,self.response.status_code))
        
    def inicializarDb(self):
        print "Inicializar DB"
        self.cliente = pymongo.MongoClient(self.config['db_conn'])
        self.db = self.cliente[self.config['db_mane']]
        self.colData = self.db[self.config['col_item']]
    
    def scrap(self):
        # TODO control de errores
        print "Iniciamps Scrap"
        procesar = True 
        paginas = self.config['paginado_inicio']
        
        while procesar:
            
            if self.config['procesar_pagina_inicial'] != False:
                print "Comenzamos a procesar paginas %s/%s" % ((paginas-1),self.totalPaginas)
                publicaciones = self.bs.select(self.config['publicaciones'])    
                # proceso la primera pagina
                for pub in publicaciones:

                    print "Procesando publicaciones"
                    # debe estar definido el campo link_detalle del config
                    if self.config['link_detalle'] != None:
                        
                        # busco el link del detalle de la publicacion
                        linkDetalle = pub.find(
                            self.config['link_detalle']['elem'],
                            class_= self.config['link_detalle']['class']
                        )[self.config['link_detalle']['attr']] 
        
                        detalleListado = self.procesarItemRaw(pub,self.config['detalle_listado'])

                        # si al link detalla debemos agregarle url_base
                        if self.config['link_detalle']['add_url_base']:
                            linkDetalle = '%s%s' % (self.config['url_base'],linkDetalle)
                        
                        print "Obteniendo el link detalle del inmueble ", linkDetalle

                        dresponse = self.sesion.get(linkDetalle, headers=self.header)
                        
                        if dresponse.status_code == 200:

                            dbs = BeautifulSoup(dresponse.content, 'html.parser')
                            detalle = self.procesarItemRaw(dbs,self.config['detalles'])

                            mergeDetalles = detalle.copy()
                            mergeDetalles.update(detalleListado)

                            item = {
                                "fecha": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                "link": linkDetalle,
                                "detalles": mergeDetalles,
                                "raw": str(pub)
                            }
                            self.colData.insert_one(item)
            else:
                # seteo bandera para volver a procesar
                self.config['procesar_pagina_inicial'] = True

            # controlamos limites
            if self.totalPaginas == paginas:
                procesar = False
                break
            else:    
                #llamamos a la siguiente pagina
                siguientePagina = self.urlPaginado % (paginas)
                print "Siguiente pagina %s"%(siguientePagina)
                self.response = self.sesion.get(siguientePagina, headers=self.header)
                if self.response.status_code == 200:
                    self.bs = BeautifulSoup(self.response.content, 'html.parser')
                    paginas+=1
                else:
                    procesar=False
                    raise ValueError(cadenas['error_response_paginado'] % siguientePagina)

    # buscar root busca en el tag root del item y solo busca attrs
    def buscarRoot(self,item,valor):
        ret = None
        # controlo la unica de entrada de configuracion que necesito attr
        if 'attr' in valor:
            try:
                ret = item[valor['attr']]
            except:
                # error aviso no existe attr
                return None    
        else:
            # error aviso la falta de param
            pass    
        return ret

    # deja valor que se ingresa como configuracion
    def modoHardCode(self,valor):
        ret = None
        # controlo la unica de entrada de configuracion que necesito s
        if 's' in valor:
            ret = valor['s']
        else:
            # error aviso la falta de param
            pass    
        return ret    
            
    def buscarFind(self,item,valor):
        ret = None
        # controlo las claves obligatorias de busqueda: elem y class
        if 'elem' in valor and 'class' in valor:
            ret = item.find(valor['elem'],class_=valor['class'])
            # si no hay resultados cortamos
            if ret == None:
                # aviso no hay resultados
                return None
            if 'attr' in valor and valor['attr'] != None:
                try:
                    ret = ret[valor['attr']]  
                except:
                    # error aviso no existe attr
                    return None    
            else:
                ret = ret.text.strip().encode(self.config['encode'])

            # una vez que tenemos resultados aplic regexp
            if 're' in valor and valor['re'] != None:
                # si no existe pos o es none
                if 'pos' not in valor or valor['pos'] == None:
                    pos = 1
                else:
                    pos = valor['pos']    
                ret = self.buscarRegExp(valor['re'],ret,pos)
                #aux = re.search(valor['re'],ret,re.I|re.M)
                #if aux != None:
                #    ret = aux.group(1)
                #else:
                #    return None    
        else:
            # error aviso la falta de param
            pass    
        return ret

    def buscarFindAll(self,item,valor):
        ret = None
        # controlo las claves obligatorias de busqueda: elem y class
        if 'elem' in valor and 'class' in valor:
            ret = item.find_all(valor['elem'],class_=valor['class']) 
            # si no hay resultados cortamos
            if ret == None:
                # aviso no hay resultados
                return None
            if 'attr' in valor and valor['attr'] != None:
                try:
                    ret = [ x[valor['attr']] for x in ret ] 
                    return ret
                except:
                    # error aviso no existe attr
                    return None    
            # una vez que tenemos resultados aplic regexp
            if 're' in valor and valor['re'] != None:
                # si no existe pos o es none
                if 'pos' not in valor or valor['pos'] == None:
                    pos = 1
                else:
                    pos = valor['pos']    
                ret = [ self.buscarRegExp(valor['re'],str(x),pos) for x in ret ]           
        else:
            # error aviso la falta de param
            pass    
        return ret

    def buscarFindAllPos(self,item,valor):
        ret = None
        # controlo las claves obligatorias de busqueda: selector
        if 'i' in valor and valor['i'] != None:
            
            ret = self.buscarFindAll(item,valor)
            if len(ret) != 0:
                if len(ret) > valor['i']: 
                    ret = ret[valor['i']].text.strip().encode(self.config['encode'])   
                    # una vez que tenemos resultados aplic regexp
                    if 're' in valor and valor['re'] != None:
                        # si no existe pos o es none
                        if 'pos' not in valor or valor['pos'] == None:
                            pos = 1
                        else:
                            pos = valor['pos']    
                        ret = self.buscarRegExp(valor['re'],ret,pos)
                        #aux = re.search(valor['re'], ret,re.I|re.M)
                        #if aux != None:
                        #    ret = aux.group(1)
                        #else:
                        #    return None       

                else:
                    # error indice fuera de rango
                    return None    
            else:
                # error aviso lista vacia
                return None    
        
        else:
            # error aviso la falta de param
            pass    
        return ret

    def buscarSelectOne(self,item,valor):
        ret = None
        # controlo las claves obligatorias de busqueda: selector
        if 'selector' in valor:
            ret = item.select_one(valor['selector']) 
            # si no hay resultados cortamos
            if ret == None:
                # aviso no hay resultados
                return None
            if 'attr' in valor and valor['attr'] != None:
                try:
                    ret = ret[valor['attr']] 
                except:
                    # error aviso no existe attr
                    return None    
            else:
                ret = ret.text.strip().encode(self.config['encode'])   

            # una vez que tenemos resultados aplic regexp
            if 're' in valor and valor['re'] != None:
                # si no existe pos o es none
                if 'pos' not in valor or valor['pos'] == None:
                    pos = 1
                else:
                    pos = valor['pos']    
                ret = self.buscarRegExp(valor['re'],ret,pos)
                #aux = re.search(valor['re'], ret,re.I|re.M)
                #if aux != None:
                #    ret = aux.group(1)
                #else:
                #    return None       

        else:
            # error aviso la falta de param
            pass    
        return ret

    def buscarSelect(self,item,valor):
        ret = None
        # controlo las claves obligatorias de busqueda: selector
        if 'selector' in valor:
            ret = item.select(valor['selector']) 
            # si no hay resultados cortamos
            if ret == None:
                # aviso no hay resultados
                return None
            if 'attr' in valor and valor['attr'] != None:
                try:
                    ret = [ x[valor['attr']] for x in ret ] 
                    return ret
                except:
                    # error aviso no existe attr
                    return None   # revisar
            # una vez que tenemos resultados aplic regexp
            if 're' in valor and valor['re'] != None:
                # si no existe pos o es none
                if 'pos' not in valor or valor['pos'] == None:
                    pos = 1
                else:
                    pos = valor['pos']    
                ret = [ self.buscarRegExp(valor['re'],str(x),pos) for x in ret ]           
        else:
            # error aviso la falta de param
            pass    
        return ret

    def buscarSelectPos(self,item,valor):
        ret = None
        # controlo las claves obligatorias de busqueda: selector
        if 'i' in valor and valor['i'] != None:
            
            ret = self.buscarSelect(item,valor)
            if len(ret) != 0:
                if len(ret) > valor['i']: 
                    ret = ret[valor['i']].text.strip().encode(self.config['encode']) 
                    # una vez que tenemos resultados aplic regexp
                    if 're' in valor and valor['re'] != None:
                        # si no existe pos o es none
                        if 'pos' not in valor or valor['pos'] == None:
                            pos = 1
                        else:
                            pos = valor['pos']    
                        ret = self.buscarRegExp(valor['re'],ret,pos)
                        #aux = re.search(valor['re'], ret, re.I|re.M)
                        #if aux != None:
                        #    ret = aux.group(1)
                        #else:
                        #    return None       

                else:
                    # error indice fuera de rango
                    return None    
            else:
                # error aviso lista vacia
                return None    
        
        else:
            # error aviso la falta de param
            pass    
        return ret

    def procesarItemRaw(self,item,config):
        #print "Entramos procesarItemRaw"
        detalles = {}
        for clave, valor in config.iteritems():

            # si el valor de la clave de config es None 
            # asigno ese valor a esa clave
            if valor == None:
                # TODO poner aviso
                detalles[clave] = None
                continue

            # comprobamos modo
            if 'modo' in valor:
                if valor['modo'] == 'root': # solo para buscar attr en item/root
                    detalles[clave] = self.buscarRoot(item,valor)
                if valor['modo'] == 'hardcode': # para hardcodear un valor
                    detalles[clave] = self.modoHardCode(valor)

                
                if valor['modo'] == 'find':
                    detalles[clave] = self.buscarFind(item,valor)
                if valor['modo'] == 'find_all':
                    detalles[clave] = self.buscarFindAll(item,valor)
                if valor['modo'] == 'find_all_pos':        
                    detalles[clave] = self.buscarFindAllPos(item,valor)
                
                if valor['modo'] == 'select_one':
                    detalles[clave] = self.buscarSelectOne(item,valor)
                if valor['modo'] == 'select':        
                    detalles[clave] = self.buscarSelect(item,valor)
                if valor['modo'] == 'select_pos':
                    detalles[clave] = self.buscarSelectPos(item,valor)    

                # control de precio
                #if clave == 'precio':
                #    detalles[clave] = detalles[clave].replace("USD", "")
                #    detalles[clave] = detalles[clave].replace(".", "")
                #    detalles[clave] = detalles[clave].replace(",", "")
                #    detalles[clave] = detalles[clave].strip()


                # chequeo no tener string vacias
                if detalles[clave] != None and len(detalles[clave]) == 0:
                    detalles[clave] = None

            else:
                # TODO poner aviso de error no hay modo
                # y setear esa clave como None
                detalles[clave] = None
       
            #print "detalle key: %s  valor: %s"%(clave,detalles[clave])
        
        return detalles

    def buscarRegExp(self,exp,s,pos):
        aux = re.search(exp,s,re.I|re.M)
        if aux != None:
            if pos == None:
                pos = 1
            return aux.group(pos)
        else:
            return None  