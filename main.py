# coding=utf-8
import threading
from core.Scrapp import Scrapp
from config.ConfigArgenprop import configArgenprop
from config.ConfigProperati import configProperati
from config.ConfigZonaprop import configZonaprop
from config.MercadoLibreAPI import MercadoLibreAPI

def main():
    hilos = list()
    hilos.append(threading.Thread(target=Scrapp, args=(configArgenprop,)))
    hilos.append(threading.Thread(target=Scrapp, args=(configProperati,)))
    #hilos.append(threading.Thread(target=Scrapp, args=(configZonaprop,)))
    hilos.append(threading.Thread(target=MercadoLibreAPI))
    for h in hilos:
        h.start()

if __name__ == '__main__':
    main()