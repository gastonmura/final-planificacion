const { Router } = require('express')
const router = Router()

// importo los controladores que machean con las rutas creadas
const inmueblesControllers = require("../controllers/Inmueble.controller") 

// definimos rutas
router.get("/count/",  inmueblesControllers.getCount)
router.get("/gets/", inmueblesControllers.getInmuebles)
router.get("/gets/:id", inmueblesControllers.getInmueble)
router.get("/export/", inmueblesControllers.getDataToExport)


module.exports =  router