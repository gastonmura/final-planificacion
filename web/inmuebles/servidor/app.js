
// https://www.youtube.com/watch?v=qf8-JzU-4IE
require("./db/db")
const express = require("express");
const inmueblesRouter = require("./routes/routes");
const app = express();
const cors = require("cors")
const PORT = process.env.PORT || 3000

app.use(cors());
// cuando defino las rutas que vamos a utilizar yo puedo indicar
// una ruta fija que preceda a toda las definidad en routes.js
// en este caso definimos /api/inmuebles/ y esta ruta se va a agregar 
// al comienzo de las rutas definidas en routes.js
app.use("/api/inmuebles",inmueblesRouter)
// para que comprenda peticiones que vienen con json
app.use(express.json())
// para que comprenda los datos qe vienen de un form html
app.use(express.urlencoded({extended:false}))

app.listen(PORT, (req, res) => {
    console.log(`aplicacion escuchando en port ${PORT}`)
})