
const mongoose = require('mongoose');
mongoose
    .connect("mongodb://localhost:27017/inmuebles_db",
    {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false
    })
    .then( (db) => console.log('MongoDB Connection Succeeded.') )
    .catch( (err) => console.log('Error in DB connection: ' + err) )
