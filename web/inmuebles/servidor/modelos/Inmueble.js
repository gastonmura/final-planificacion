const mongoose = require('mongoose');

const Inmueble = mongoose.model('Inmueble', {
    raw: {
        type: String,
        required: true
    },
    fecha: {
        type: String,
        required: true
    },
    link: {
        type: String,
        required: true
    },
    
});
module.exports = Inmueble