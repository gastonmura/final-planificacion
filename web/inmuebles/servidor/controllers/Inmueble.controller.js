// defino el objeto que va a contener las funciones que atenderan las rutas definidas
const inmuebleController = {}

const Inmueble = require("../modelos/Inmueble")

// Defino funciones que atienden
inmuebleController.getCount = async (req, res) => {
    //const total = await Inmueble.estimatedDocumentCount()
    const total = await Inmueble.find({$and: [
        { $or: [{"detalles.operacion": "VENTA" }, { "detalles.operacion": "Venta" } ] },
        { $or: [{"detalles.tipologia": "Departamento" }, { "detalles.tipologia": "Casa" } ] },
        ]},{_id:1}).count()
    res.json(total)
}

inmuebleController.getInmuebles = async (req, res) => {

    let pagina = Number.parseInt(req.query['pagina'])
    let porPagina = Number.parseInt(req.query['porPagina'])
    let salto = 0

    if( pagina != 0 )
        salto = porPagina + pagina
              
    const inmuebles = await Inmueble.find({$and: [
        { $or: [{"detalles.operacion": "VENTA" }, { "detalles.operacion": "Venta" } ] },
        { $or: [{"detalles.tipologia": "Departamento" }, { "detalles.tipologia": "Casa" } ] },
        ]}).limit(porPagina).skip( salto )
    res.send(inmuebles)
}

inmuebleController.getInmueble = async (req, res) => {
    console.log(req.params)
    const i = await Inmueble.findById(req.params.id) 
    res.send(i)
}


inmuebleController.getDataToExport = async (req, res) => {
    let item = []
    const { Parser } = require('json2csv')
    const inmuebles = await Inmueble.find({$and: [
        { $or: [{"detalles.operacion": "VENTA" }, { "detalles.operacion": "Venta" } ] },
        { $or: [{"detalles.tipologia": "Departamento" }, { "detalles.tipologia": "Casa" } ] },
        ]},{_id:0,detalles:1,detalles:{images:0}})
    
    inmuebles.forEach(
        elem => {
            item.push(elem.toJSON().detalles)
        }
    )

    fields = ["id", "tipologia", "direccion", "fecha_acceso", "fuente", "ambientes", "titulo", "banios", "lote_m2", "m2_cubiertos", "descripcion", "precio", "fecha_publicacion", "lat" , "long" , "vendedor", "operacion" , "dormitorios" , "antiguedad" , "agua" , "electricidad" , "gas" , "cloacas" , "pileta" , "cochera" , "luz" , "telefono" , "sup_terreno" , "moneda", "pais" , "provincia" , "partido"  , "localidad" , "soporte" , "barrio" , "uso_real" , "uso_potencial" , "positivos" , "negativos" , "ubicacion" , "constructora" , "financiamiento" , "edificio" , "zona_segun_pdu" , "fot" , "fos" , "amenidades" , "orientacion" , "distancia_centro", "incidencia_precio_suelo_precio_total" , "registro_unico"]
    
    const json2csv = new Parser({fields});
    const csv = json2csv.parse(item);
    res.header('Content-Type', 'text/csv');
    res.attachment("export-data.csv");
    res.send(csv);
}


module.exports = inmuebleController
