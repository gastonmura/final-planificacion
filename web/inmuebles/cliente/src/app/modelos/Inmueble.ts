export class Inmueble {
  constructor(_id = "", raw = "", fecha = "", link = "") {
    this._id = _id;
    this.raw = raw;
    this.fecha = fecha;
    this.link = link;
  }

  _id: string;
  raw: string;
  fecha: string;
  link: string;
}