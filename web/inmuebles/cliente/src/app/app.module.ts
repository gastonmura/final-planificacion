import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BuscadorComponent } from './buscador/buscador.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InmueblesService } from './servicios/inmuebles.service';

import { PrimengModule } from './primeng/primeng.module';
import { MapaComponent } from './mapa/mapa.component';
import { GraficosComponent } from './graficos/graficos.component';
import { ExportarComponent } from './exportar/exportar.component';
import { AcercadeComponent } from './acercade/acercade.component';
import { AppRoutinModule } from './app-routin.module'

@NgModule({
  declarations: [
    AppComponent,
    BuscadorComponent,
    MapaComponent,
    GraficosComponent,
    ExportarComponent,
    AcercadeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    PrimengModule,
    HttpClientModule,
    AppRoutinModule
  ],
  providers: [InmueblesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
