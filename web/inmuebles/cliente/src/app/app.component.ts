import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  items: MenuItem[];
  
  ngOnInit() {
    this.items = [
      {label: 'Inicio', icon: 'pi pi-fw pi-home', routerLink: ['/inicio']},
      {label: 'Mapa', icon: 'pi pi-fw pi-map', routerLink: ['/mapa']},
      {label: 'Graficos', icon: 'pi pi-fw pi-chart-line', routerLink: ['/graficos']},
      {label: 'Exportar', icon: 'pi pi-fw pi-download', routerLink: ['/exportar']},
      {label: 'Acerca de...', icon: 'pi pi-fw pi-user', routerLink: ['/acercade']}
  ];
  } 
}
