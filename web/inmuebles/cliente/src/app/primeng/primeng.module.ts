import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuModule } from 'primeng/menu';
import { ButtonModule } from 'primeng/button';
import {TabMenuModule} from 'primeng/tabmenu';
import {DataViewModule} from 'primeng/dataview';
import {CardModule} from 'primeng/card';
import {ChartModule} from 'primeng/chart';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MenuModule,
    ButtonModule,
    TabMenuModule,
    DataViewModule,
    CardModule,
    ChartModule
  ],
  exports: [
    MenuModule,
    ButtonModule,
    TabMenuModule,
    DataViewModule,
    CardModule,
    ChartModule
  ],
})
export class PrimengModule { }


