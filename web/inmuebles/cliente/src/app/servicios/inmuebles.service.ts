import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' 
import { Inmueble } from '../modelos/Inmueble' 


@Injectable({
  providedIn: 'root'
})
export class InmueblesService {

  urlApi = "http://localhost:3000/api/inmuebles/"
  inmuebles: Inmueble[];
  
  constructor(private http: HttpClient) { }

  getInmuebles(pagina:string, porPagina:string){
    const params = {
      pagina: pagina,
      porPagina: porPagina
    }
    return this.http.get<Inmueble[]>(this.urlApi + "gets/", {params}) 
  }
  
  getCount(){    
    return this.http.get(this.urlApi + "count/") 
  }

  downloadData(){    
    return this.http.get(this.urlApi + "export/") 
  }

}
