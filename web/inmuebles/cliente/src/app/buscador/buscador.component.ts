import { Component, OnInit } from '@angular/core';
import { InmueblesService } from '../servicios/inmuebles.service';
import { Inmueble } from '../modelos/Inmueble' 

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  constructor(public inmueblesService: InmueblesService) { }

  inmuebles: Inmueble[];
  total: number;
  pagina: number;
  porPagina: number;

  ngOnInit(): void {   
    this.pagina = 0;
    this.porPagina = 21;
    this.totalInmuebles();
    this.parseInmuebles(); 
  }

  loadData(event) {
    this.pagina = event.first
    this.porPagina = event.rows  
    this.parseInmuebles()
  }

  totalInmuebles(){
    this.inmueblesService.getCount().subscribe(
      res => {
       this.total = Number.parseInt(res.toString())
      },
      err => console.log(err)
    )
  }

  parseInmuebles(){
    this.inmueblesService.getInmuebles(this.pagina.toString(),this.porPagina.toString()).subscribe(
      res => {
       this.inmuebles = res
      },
      err => console.log(err)
    )
  }

}
