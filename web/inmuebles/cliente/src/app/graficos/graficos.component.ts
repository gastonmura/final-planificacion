import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graficos',
  templateUrl: './graficos.component.html',
  styleUrls: ['./graficos.component.css']
})
export class GraficosComponent implements OnInit {


  data: any;
  options: any;

  constructor() {
    this.valorPromedioUsdXCiudad()
  }

  valorPromedioUsdXCiudad(){
    this.data = {
      labels: [
        "Rada Tilly",
        "Futaleufú",
        "Rawson",
        "Trevelin",
        "Lago Puelo",
        "Epuyen",
        "Cushamen",
        "Costa Chubut",
        "Tehuelches",
        "Sierra Cuadrada",
        "Esquel",
        "Sarmiento",
        "Valle Garin",
        "El Hoyo",
        "Biedma",
        "Languiñeo",
        "Cholila",
        "Escalante",
        "Comodoro Rivadavia",
        "Puerto Madryn"  
      ],
      datasets: [
          {
              data: [
                326229.16,
                166587.30,
                227437.5,
                350000,
                196300,
                80000,
                258558.82,
                650000,
                89000,
                0.0,
                140000,
                90000,
                114105.26,
                222266.66,
                109804.76,
                52000,
                49500,
                142352.94,
                250138.88,
                92327.27
              ],
              backgroundColor: [
                  "#FF6384",
                  "#3633EB",
                  "#FF2356",
                  "#FF6384",
                  "#3562EB",
                  "#FFCE99",
                  "#FFAA84",
                  "#3CCCEB",
                  "#FF5056",
                  "#FF2184",
                  "#3905EB",
                  "#FFAA56",
                  "#FF0084",
                  "#36A290",
                  "#F11116",
                  "#FA2344",
                  "#3DE45B",
                  "#FF0986",
                  "#FFCCAB",
                  "#360000"
              ],
              hoverBackgroundColor: [
                  "#FF6384",
                  "#3633EB",
                  "#FF2356",
                  "#FF6384",
                  "#3562EB",
                  "#FFCE99",
                  "#FFAA84",
                  "#3CCCEB",
                  "#FF5056",
                  "#FF2184",
                  "#3905EB",
                  "#FFAA56",
                  "#FF0084",
                  "#36A290",
                  "#F11116",
                  "#FA2344",
                  "#3DE45B",
                  "#FF0986",
                  "#FFCCAB",
                  "#360000"
              ]
          }]    
      };
    
    this.options = {
      title: {
          display: true,
          text: 'Valor Promedio de Inmuebles por Ciudad',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }  
    };  

  } 



  ngOnInit(): void {
  }

}
