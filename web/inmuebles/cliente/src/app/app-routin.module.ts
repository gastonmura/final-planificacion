import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router'; 
import { MapaComponent } from './mapa/mapa.component';
import { GraficosComponent } from './graficos/graficos.component';
import { ExportarComponent } from './exportar/exportar.component';
import { AcercadeComponent } from './acercade/acercade.component';
import { BuscadorComponent } from './buscador/buscador.component';

const routes:Routes = [
    { path: "", redirectTo:"/inicio", pathMatch: "full" },
    { path: "inicio", component: BuscadorComponent },
    { path: "mapa", component: MapaComponent },
    { path: "graficos", component: GraficosComponent },
    { path: "exportar", component: ExportarComponent },
    { path: "acercade", component: AcercadeComponent },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutinModule { }
